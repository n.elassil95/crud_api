const { name } = require("ejs");
const express = require("express");
const path = require("path");
const sqlite3 = require("sqlite3").verbose();
// Création du serveur Express
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json())
// Configuration du serveur
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "public")));
 /////////////////////////
const db_name = path.join(__dirname, "data", "apptest.db");
const db = new sqlite3.Database(db_name, err => {
	if (err) {
		return console.error(err.message);
	}
	console.log("Connexion réussie à la base de données 'apptest.db'");
});
 /////////////////////////
const sql_create = `CREATE TABLE IF NOT EXISTS "listeEtudient" (
	"idEtud"	INTEGER NOT NULL,
	"nom"	VARCHAR(50),
	"prenom"	VARCHAR(50),
	"dateNaissance"	DATE,
	"age" INTEGER,
	"numCI"	INTEGER,
	PRIMARY KEY("idEtud" AUTOINCREMENT)
);`;

   /////////////////////////
db.run(sql_create, err => {
	if (err) {
		console.log("2");
		return console.error(err.message);
	}
	console.log("Création réussie de la table 'etudients'");
});

 /////////////////////////
// Alimentation de la table
/*const sql_insert = `INSERT INTO listeEtudient(idEtud,nom,Prenom,dateNaissance,numCI) VALUES
   ('EL ASSIL', 'Noure2', "15/01/1955" ,68, 135663);`;
db.run(sql_insert, err => {
	if (err) {
		//console.log("3");
		return console.error(err.message);
	}

	console.log("Alimentation réussie de la table 'etudients'");
});*/
// Démarrage du serveur
app.listen(3000, () => {
	console.log("Serveur démarré (http://localhost:3000/) !");
});
 /////////////////////////
// GET /
app.get("/", (req, res) => {
	// res.send("Bonjour le monde...");
	res.render("Accueil" , {model : ""});
});
 /////////////////////////

//Get /data/1?recherche =?
//data/:page
app.get("/data/:page", (req, res) => {
	let recherche= req.query.recherche;
	let rowsPerPage = 5;
	let page = req.params.page;
	let offset = (page - 1) * rowsPerPage;
	const sql = "SELECT * FROM listeEtudient WHERE nom = ? OR prenom =? OR dateNaissance=? OR numCI=? ORDER BY prenom  LIMIT ? OFFSET ? ";
	db.all(sql, [recherche,recherche,recherche,recherche,rowsPerPage, offset], (err, rows) => {
		if (err) {
			console.log("hi page");
			return console.error(err.message);
		}
		//console.log(offset);
		//res.render("data", { model: rows ,page:parseInt(page),recherche:recherche});
		res.json(rows)
	});
});
//////////////
//Get /liste de touts la Base de données
app.get("/liste", (req, res) => {

	const sql = "SELECT * FROM listeEtudient ORDER BY prenom";
	db.all(sql, [], (err, rows) => {
		if (err) {
			console.log("hi page");
			return console.error(err.message);
		}
		//res.render("liste", { model: rows });
		res.json({model: rows });
	});
});
 /////////////////////////
function getAge(date) { //Convertir la date de naissance en âge.

	var diff = Date.now() - new Date(date).getTime();
	var age = new Date(diff);
	return Math.abs(age.getUTCFullYear() - 1970);
}
 /////////////////////////
// GET /create
app.get("/create", (req, res) => {
	res.render("create", { model: {} });
});
 /////////////////////////
// POST /create
app.post("/create", (req, res) => {
	const sql = "INSERT INTO listeEtudient(nom,Prenom,dateNaissance,age,numCI) VALUES (?, ?, ?,?,?)";
	//const sql2= "UPDATE listeEtudient SET idEtud = idEtud - 1 WHERE idEtud > ?";
	const age=getAge(req.body.dateNaissance);

	//const params = [3];
	const etudiant = [req.body.nom, req.body.prenom, req.body.dateNaissance,age, req.body.num];
console.log(etudiant);
	db.run(sql, etudiant, (err) => {
			
		/*db.all(sql2, params, (err) => {
  
			if (err) {
			  console.log("7");
			  return console.error(err.message);
		
			}*/
		
	  if (err) {
		console.log("6");
		return console.error(err.message);
  
	  }
  
		}); 
	//}); 
	//res.redirect('/liste');  
	res.json({message:"ligne etait créé"});

});
 /////////////////////////
// GET /edit/5
/*Sapp.get("/index/:id", (req, res) => {
	const id = req.params.id;
	const sql = "SELECT * FROM listeEtudient WHERE idEtud = ?";
	//console.log(id)
	db.get(sql, id, (err, row) => {
		if (err) {
			console.log("5");
			return console.error(err.message);
		}
		//res.render("index", { model: row });
		res.json(row)
	});
});*/
 /////////////////////////
// POST /edit/5
app.post("/index/:id", (req, res) => {
	const id = req.params.id;
	const age=getAge(req.params.dateNaissance);
	const etudient = [req.body.nom, req.body.prenom, req.body.dateNaissance,age ,parseInt(req.body.num), id];
	const sql = "UPDATE listeEtudient SET nom = ?, prenom = ?, dateNaissance = ? ,age=?,numCI = ? WHERE (idEtud = ?)";
	console.log(etudient)
	//console.log(req)
	console.log(req.body.numCI)
	db.run(sql, etudient, err => {
		if (err) {
			//console.log("5");
			return console.error(err.message);
		}
		//res.redirect("/liste");
		res.json()
	});
});
 /////////////////////////
app.get("/suppEtudiant/:id", (req, res) => {

	const sql = "DELETE from listeEtudient WHERE idEtud = ?";
	const sql2= "UPDATE listeEtudient SET idEtud = idEtud - 1 WHERE idEtud > ?";
	const params = [req.params.id];
  
	db.run(sql, params, (err) => {
		
		db.run(sql2, params, (err) => {
  
			if (err) {
			  console.log("7");
			  return console.error(err.message);
		
			}
		
	  if (err) {
		console.log("6");
		return console.error(err.message);
  
	  }
  
		}); 
	}); 
	//res.redirect('/liste');  
  res.json();
  });
 /////////////////////////
  /*app.post("/rechercheEtudient", (req, res) => {

	const sql = "SELECT * FROM listeEtudient WHERE nom = ? OR prenom =? OR dateNaissance=? OR numCI=? ";
	const params = [req.body.recherche,req.body.recherche,req.body.recherche,req.body.recherche];
  console.log(params);
	db.all(sql, params, (err,rows) => {
  
	  if (err) {
		console.log("6");
		return console.error(err.message);
  
	  }
	  res.render("data", { model: rows });
	}); 
  
	
  
  });*/
   /////////////////////////
